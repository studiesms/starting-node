# Starting Node

Uma pequena descrição do projeto. O intuíto é ajudar aqueles que realmente querem aprender a como criar uma API em NODEJS, todo esse esforço e trabalho é apenas para auxiliar vocês, assim ajudando a crescer e desenvolver cada vez mais nossa comunidade, sem mais. 

Sejam Bem-Vindos ao NodeJS! 

> Lembre-se de instalar o Node em sua máquina antes!

## Build Setup

``` bash
# instalando dependências
npm install

# iniciando servidor
node ./bin/server.js

# inciando servidor com NODEMON (recomendando)
nodemon .

# iniciando servidor para production
pm2 start ./bin/sever.js

# reiniciando servidor na production
pm2 restart all

```
> Quando for criar um arquivo novo, instale as seguintes dependencias: express, nodemon, body-parser, cors, mysql (caso for usar MySQL, ou pode ser mongoose para MongoDb), util, caso queria adicionar mais procure no NPM (site com varias libs para utilizar em seus projetos NODE)