CREATE DATABASE IF NOT EXISTS tai_nodejs;

USE tai_nodejs;

CREATE TABLE IF NOT EXISTS user_tai (
	id INT AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO user_tai (nome, email) VALUES 
('João das Neves', 'joao@norte.com'),
('Arya Stark', 'ninguem@norte.com'),
('Sansa Stark', 'sonsa@norte.com'),
('Tony Stark', 'genio@playboy.com'),
('Morgan Stark', 'mil@milhoes.com');
