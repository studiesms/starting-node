// Lib para tratar diferentes retornos nas chamadas
// JSEND -> chamadas com retornos de sucesso
// JERROR -> chamadas com retornos de erro
require('express-jsend')
  
let express = require('express')
let router = express.Router()

// importando rotas externas, lembre-se que este arquivo é a raíz das rotas, para rotas alternativas e subrotas
// o recomendado é inserir em aqrquivos externos (separados)
let users = require('./users')
router.use('/users', users) 

// como definir uma rota, esta rota é a padrão, mais basica, geralmente para testar sua API
// rota: http://localhost:3000 
router.get('/', async (req, res) => {
  try {
    res.json({
      msg: 'Welcome to API - Studies about NodeJS :)',
    })
  } catch (err) {
    res.jerror(err)
  }
})

// rota para tratamento de erros da API
router.use((err, req, res, next) => {
  if (err) {
    res.status(500)
    res.jerror(err)
  }
})

module.exports = router