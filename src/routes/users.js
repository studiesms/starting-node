require('express-jsend')

let express = require('express')
let router = express.Router()

//  importando o controller para inserir seus metodos nas rotas abaixo
const UsersController = require('../controllers/users')

// Sem muito segredo, apenas instanciando o controller e chamado seu método
// após retornando o resultado do controller para a chamada
// Obs: Lembra dos throw new Error, então eles servem para que quando der algum erro, entrar no catch abaixo
// bem importante manter o try, catch nas rotas
router.get('/', async (req, res) => {
  try {
    let userController = new UsersController(req.db)
    let allUsers = await userController.get(req, res)

    res.jsend(allUsers)

  } catch (err) {
    res.jerror(err)
  }
})

router.get('/:id', async (req, res) => {
  try {
    let userController = new UsersController(req.db)
    let users = await userController.getById(req, res)

    res.jsend(users)

  } catch (err) {
    res.jerror(err)
  }
})

router.post('/', async (req, res) => {
  try {
    let userController = new UsersController(req.db)
    let userId = await userController.insert(req, res)

    res.jsend(userId)

  } catch (err) {
    res.jerror(err)
  }
})

// exemplos de outro métodos para se utilizar na api
// PUT -> quando for fazer algum update em algum registro no banco
// DELETE -> quando for apagar algum registro no banco


router.put('/', async (req, res) => {
  res.send('Put Route')
})

router.delete('/', async (req, res) => {
  res.send('Delete Route')
})

module.exports = router