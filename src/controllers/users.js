// Importando o Model para instanciar a sua classe e acessar seus métodos de acesso ao banco
const UsersModel = require('../models/tables/users')

class Users {

  // No construtor abaixo eh recebido como parametro a conexao com o banco
  // literalmente a conexao em uma variavel, com isso será passado à instancia
  // da classe do MODEL (classe que utiliza a conexão ao banco), como pode ver na linha 11
  // portanto a variavel this.model (variavel da classe) é a instancia do MODEL
  constructor(con) {
    this.model = new UsersModel(con)
  }

  //  Aqui estão os métodos (comum em Oriantada a objetos), este métodos que serão chamados
  //  nas rotas de acordo com sua necessidade, eles sempre precisam receber o parametro req e res
  //  neste exemplo não se tem muita utilidade, mas pode se fazer MUITA coisa com esses parametros
  //  através de middlewares e helpers, só da uma pesquisada que acha muita coisa boa, um exemplo é
  //  é a conexão do banco que passo no "req.db", pode-se usar muita coisa no req, como filtros e funções
  async get (req, res) {

    // chamada ao model para utilizar o método getAll do model
    let allUsers = await this.model.getAll()

    // tratamento dos dados, caso queira usar....(recomendado)

    return allUsers

  }

  async getById (req, res) {

    //  diferente da chamada acima que é apenas um getAll sem nenhuma especificação
    //  esta chamda é especifica pois seus intuito é buscar apenas um user pelo seu Id
    //  para isso na rota é inserido um parametro, ex: 
    //   - localhost:3000/users/1 -> o 1 é o param denominado na rota como ":id"
    
    // Há outros exemplos de se usar passagem de valores em chamdas get, como a query (URL amigáveis) ex: 
    //   - localhost:3000/users?id=1 -> neste caso o 1 esta na query da rota, e não há necessidade de inserir ele na rota como o param
    // para pegar este valor de query utiliza req.query.id
    

    // Aqui esta sendo feita uma verificação, caso o req.params.id não ter nenhum valor (null) ele acionará um erro personalizado
    // esta é a função do new Error, ele cria um erro personalizado. O throw serve para bloquear o processo do método, o new Error para 
    // definir um erro. Desta forma ele para o processo nesta linha e VOLTA no "catch" das rotas. 
    // É muito importante utilizar estas verificações para tratar seus erros
    if (!req.params.id) throw new Error('Params is not defined.')
    let id = parseInt(req.params.id)

    // nesta linha esta sendo chamado o método do banco getById e nota-se que desta vez esta sendo passado um parametroao model
    // este que é o Id do usuário que queremos pegar as informações
    // Observa-se que desta vez a variavel user esta entre [user], esta é uma opção do JS
    // que serve para pegar o objeto user dentro do array que esta sendo retornado em "await this.model.getBydId(id)"
    // caso não use o [user] e deixe apenas user o seu resultado será uma array com o objeto de resposta dentro
    let [user] = await this.model.getBydId(id)
    
    // mais uma verificação, isto é bem importante em todo processo de uma API, asism evita erros de alteração de dados em seu BD
    // ou erros de retornos vazios nas rotas, lembre-se: 
    // SE DER ERRO DEVE RETORNAR ERRO!!!!!
    if (!user) throw new Error('Resource not found.')
    
    // tratamento dos dados, caso queira usar....(recomendado)

    // retorno dos dados
    return user

  }

  async insert (req, res) {
    // Diferente dos demaies este é um método POST, ele é usado para inserir dados no BD
    // por conta disso na maioria das vezes deverá passar um grande numero de informações, como um formulário de cadastro por exemplo
    // algo que dificultaria passar por parametro (muito parametros não dá né!)
    // por conta disso tem um outro método de pssagem de valores, que se chama Body request,
    // para pegar os valores do body, utiliza-se req.body

    // no caso abaixo estou verificando se o objeto user esta no body da requisição,
    // caso ele não esteja não posso continuar o código por conta disso COLOCA O THROW NEW ERROR ai kkk
    if (!req.body.user) throw new Error('Body is not defined.')

    // outa artemanha legal de se fazer é essa facilidade para pegar dados de um objeto, vamos-la...
    // este é o valor do req.body.user = { nome: "teste nome", email: "teste email" }
    // se colocasse dessa forma: let user = req.body.user
    // a variavel user ficaria assim: user = { nome: "teste nome", email: "teste email" }
    // para facilitar, eu crio uma variavel nome email dentro de chaves, isso significa que estarei pegando
    // os valores de nome do req.body.user e inserindo na variavel nome e assim por diante
    let { nome, email } = req.body.user

    let insert = await this.model.insertUser(nome, email)
    
    if (insert.insertId) return { id: insert.insertId }
    else throw new Error('Insert error.')
  }
}

module.exports = Users