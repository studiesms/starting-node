'use strict'
const util = require('util')


// Aqui esta exportando uma função apenas, esta escrito utilizando arrow functions
// recebe como parametro pool

module.exports = pool => (req, res, next) => {
  
  // o getConnection é a função para pegar a conexção do banco, a variavel pool se refere a pool de conexões que foi feita
  pool.getConnection((err, con) => {

    // tratamento de erros em relação ao banco
    if (err) {
      if (err.code === 'PROTOCOL_CONNECTION_LOST') {
        console.error('\x1b[31m[db]\x1b[0m Database connection was closed.')
      }
      if (err.code === 'ER_CON_COUNT_ERROR') {
        console.error('\x1b[31m[db]\x1b[0m Database has too many connections.')
      }
      if (err.code === 'ECONNREFUSED') {
        console.error('\x1b[31m[db]\x1b[0m Database connection was refused.')
      }
      if (err.code === 'ETIMEDOUT') {
        console.error('\x1b[31m[db]\x1b[0m Database connection timeout.')
      }
      if (err.code === 'PROTOCOL_SEQUENCE_TIMEOUT') {
        console.error('\x1b[31m[db]\x1b[0m Handshake inactivity timeout.')
      }
      else console.log(err)
    }

    // caso tenha uma conexção correta irá inserir esta conexão ao "req.db"
    // e adicionar a função req.db.query (que executa as queries no banco) como uma promessa
    // feito isso poderá usar o await (pesquise depois sobre promessas em NODE,caso não conheça ainda)
    if (con) {
      console.log('\x1b[36m[db]\x1b[0m Conexão Realizada')
      req.db = con
      req.db.query = util.promisify(req.db.query)

      // o next é responsável para dar continuidade a chamada,
      // esta função ocorre toda vez que uma requisição é feita a API, portanto ESTA FUNÇÃO AQUI é um Middlewares
      // eles são funções que ocorrem antes de terminar a chamada, com o intuito de adicionar méotodos ou tratar dados
      //  para dar continuidade usa-se o next(), caso contrário a chamada iria terminar neste ponto
      next()
    } else {
      console.log(err)
      next(err)
    }
    
    // tratamento apenas quando a pool é finalizada com ÊXITO!!! caso dê erro deverá ser tratado de outra forma...
    res.on('finish', () => {
      if (req.db) req.db.release()  
      else console.error('\x1b[31m[db]\x1b[0m Database connection error.')
    })
  })
}