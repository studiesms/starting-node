// Lib usada para configurar o servidor
let express = require('express')

// necessário para trabalhar com parametros e JSON
let bodyParser = require('body-parser')

// Importando o arquivo com as rotas configruadas
let router = require('../src/routes/index')

// Lib para liberar o CORS nas requisições, bem abrangente, pode usar WhiteLIst e Blacklist caso queira (desnecessário, mas pode usar)
let cors = require('cors')

let app = express()

// importando pool e passando para criar a conexão
let pool = require('./models/pool.js')
let ConnectionDataBase = require('./middlewares/connection')

// usando o Middleware criado
app.use(ConnectionDataBase(pool)) 

// aplicando no servidor...
app.use(cors())
app.options('*', cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(router)

// exportando configruação do servidor
module.exports = app