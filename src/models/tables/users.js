class Users {
  constructor(con) {
    this.db = con
    this.table = 'user_tai'
  }


  // Métodos para executar no banco através da conexão realizada
  async getAll() {
    let query = 'SELECT * FROM ' + this.table
    return await this.db.query(query)
  }

  // observa-se que neste método há um parametro, quando for utilziar parametros na query (através do WHERE)
  // coloque na query um ?, e no this.db.query, coloque dentro de um array [] a variavel que substituirá o "?" na query
  // É NA MESMA ORDEM QUE OS ? NA QUERY
  
  async getBydId(id) {
    let query = 'SELECT * FROM ' + this.table + ' WHERE id=?'
    return await this.db.query(query, [id])
  }

  async insertUser(nome, email) {
    let query = 'INSERT INTO ' + this.table + ' (nome, email) VALUES (?, ?)'
    return await this.db.query(query, [nome, email])
  }
}

module.exports = Users