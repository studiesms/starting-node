'use strict'

// instancias das libs e do arquivo de config Global do Projeto, com as variaveis de ambiente (Env.JSON)
const mysql = require('mysql')
let config = require('../../bin/env')

let db

// função para setar as variaveis de conexão ao banco
// utiliza-se as variaveis de eambiente do env.json e o status de ambiente do Node
// NODE_ENV, lembre-se de setar corretamente esta variavel, ex: export NODE_ENV = development ou production

let configDataBase = () => {
  let _db = config.get('db')
  let result = {}
  
  let environment = process.env.NODE_ENV || 'development'
  
  if (environment == 'development') {
    db = 'development'
    result = {
      host: _db.develop.host,
      user: _db.develop.user,
      port: _db.develop.port,
      password: _db.develop.password,
      database: _db.develop.database
    } 
  }
  else {
    db = 'production'
    result = {
      host: _db.production.host,
      user: _db.production.user,
      port: _db.production.port,
      password: _db.production.password,
      database: _db.production.database
    } 
  }
  
  return { 
    ...result, 
    connectionLimit : 150,
    connectTimeout  : 60 * 60 * 1000,
    acquireTimeout  : 60 * 60 * 1000,
    timeout         : 60 * 60 * 1000,
  }
}

// Fazer uma conexão ao banco de dados é muito caro para uma aplicação, por isso não fazemos uma conexão para cada request
// desta forma criamos uma unica conexão direta ao servidor do banco e a partir disso criamos uma pool de seções
// como se fosse várias ramificações da mesma conexão, onde as requests usam conforme uma fila

// criando a pool de secoes, na conexao do banco
const pool = mysql.createPool(configDataBase())
console.log(`\x1b[32m[db]\x1b[0m Conexão no banco da ${db}`)
console.log('\x1b[32m[db]\x1b[0m Pool criada!')


// listeners da pool

// Release é a função para retornar a seção utilizada a pool
// quando a seção retorna executará a função abaixo
pool.on('release', () => console.log('\x1b[36m[db]\x1b[0m Conexão Retornada a Pool'))

pool.on('SIGINT', () => {
  pool.end(err => {
    if (err) {
      return console.log('\x1b[31m[db] ' + err.message + '\x1b[0m')
    }
    console.log('\x1b[36m[db]\x1b[0m Pool Fechada.')
    process.exit(0)
  })
})

// exportando a pool
module.exports = pool

