const nconf = require('nconf')

// criando um ambiente com variaveis globais, de acordo com o env.json
// no arquivo env.json é o environment (ambiente) da aplicação
nconf.argv()
  .env()
  .file({ file: './env.json' })

module.exports = nconf