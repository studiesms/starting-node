// importando configuração do servidor e variaveis de ambiente
const app = require('../src/app')
const env = require('./env')

// buscando o valor da porta que foi definida no environment
let port = env.get('port')

// ligando servidor :)
app.listen((port || process.env.PORT) , () => {
  console.log('\x1b[32m[api]\x1b[0m API - Studies about NodeJS')
  console.log('\x1b[32m[api]\x1b[0m API - PORT: ' + (port || process.env.PORT))    
})

